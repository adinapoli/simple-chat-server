import log from 'loglevel';
import net from 'net';
import { v4 } from 'uuid';
import { Client } from './client.js';

const generateUUID = () => v4();

export class ChatServer {
  #clients = [];
  #server;

  constructor() {
    const onClientSendData = (client, data) => {
      this.#broadcast(
        `${client.nickname}> ${data.toString()}`,
        client.nickname
      );
    };

    const onClientDisconnect = (client) => {
      this.#remove(client);

      const logoutMessage = `${client.nickname} left this chat\n`;
      this.#broadcast(logoutMessage, client.nickname);
      log.info(logoutMessage);
    };

    const onClientError = (client, error) => {
      log.error(
        `An error occurs on client ${client.nickname}: ${error.message}`
      );
    };

    const onClientConnect = (socket) => {
      const clientName = `Guest-${generateUUID()}`;
      this.#clients.push(
        new Client(
          clientName,
          socket,
          onClientDisconnect,
          onClientError,
          onClientSendData
        )
      );
      const loginMessage = `${clientName} joined this chat.\n`;
      this.#broadcast(loginMessage, clientName);
      log.info(loginMessage);
    };

    this.#server = net.createServer(onClientConnect);

    this.#server.on('error', (error) => {
      log.error(`An error occurs on client: ${error.message}`);
    });
  }

  async start(port = 10000) {
    this.#server.listen(port, () => {
      log.info(`Server is listening at http://localhost:${port}`);
    });
  }

  async close() {
    this.#server.close(() => {
      log.info(`Server closed`);
    });
  }

  #broadcast(aMessage, from) {
    this.#clientsExcept(from).map((client) => client.write(aMessage));
  }

  #remove(aClient) {
    this.#clients.splice(this.#clients.indexOf(aClient), 1);
  }

  #clientsExcept(aClientName) {
    return this.#clients.filter((client) => client.nickname !== aClientName);
  }
}
