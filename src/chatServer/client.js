export class Client {
  static #ERROR_EVENT = 'error';
  static #CLIENT_SEND_DATA_EVENT = 'data';
  static #CLIENT_DISCONNECT_EVENT = 'end';

  #socket;

  constructor(
    nickname,
    socket,
    onClientDisconnect,
    onClientError,
    onClientSendData
  ) {
    this.nickname = nickname;
    this.#socket = socket;

    socket.write('You are connected to simple-chat-server.\n');

    socket.on(Client.#CLIENT_SEND_DATA_EVENT, (data) =>
      onClientSendData(this, data)
    );

    socket.on(Client.#CLIENT_DISCONNECT_EVENT, () => onClientDisconnect(this));

    socket.on(Client.#ERROR_EVENT, (error) => onClientError(this, error));
  }

  write(aMessage) {
    this.#socket.write(aMessage);
  }
}
