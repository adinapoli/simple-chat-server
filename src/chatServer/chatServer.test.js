import { ChatServer } from './chatServer.js';
import net from 'net';

const PORT = 10000;

describe(`Chat server incoming connections on port ${PORT}`, () => {
  it('should accept a client when it has been started', (done) => {
    const chatServer = new ChatServer();
    chatServer.start().then(() => {
      const socket = net.createConnection(PORT, () => {
        expect(socket.readable).toBe(true);
        socket.end(() => chatServer.close().then(() => done()));
      });
    });
  });

  it('should not accept a client when it has been closed', (done) => {
    const chatServer = new ChatServer();
    chatServer.start().then(() => {
      chatServer.close().then(() => {
        const socket = net.createConnection(PORT);
        socket.on('error', (error) => {
          expect(error).not.toBe(null);
          expect(error.code).toBe('ECONNREFUSED');
          done();
        });
      });
    });
  });
});

describe('Sender write a single line of test, server broadcasts that line to all other connected clients', () => {
  test('the other chat partecipant will receive the message', (done) => {
    const chatServer = new ChatServer();
    const messageToSend = 'Test Message';
    const receiverMessages = [];

    chatServer.start().then(() => {
      const receiver = net.createConnection(PORT);
      receiver.on('data', (data) => {
        receiverMessages.push(data.toString());
      });

      const sender = net.createConnection(PORT, () => {
        sender.write(messageToSend);
        sender.end(() => {
          receiver.end(() => {
            chatServer.close().then(() => {
              try {
                expect(
                  receiverMessages.some(
                    (message) => message.indexOf(messageToSend) > -1
                  )
                ).toBe(true);
                done();
              } catch (error) {
                done(error);
              }
            });
          });
        });
      });
    });
  });

  test("the sender won't receive the message", (done) => {
    const chatServer = new ChatServer();
    const messageToSend = 'Test Message';

    const senderMessages = [];

    chatServer.start().then(() => {
      const receiver = net.createConnection(PORT);

      const sender = net.createConnection(PORT, () => {
        sender.on('data', (data) => {
          senderMessages.push(data.toString());
        });
        sender.write(messageToSend);

        sender.end(() => {
          receiver.end(() => {
            chatServer.close().then(() => {
              try {
                expect(
                  senderMessages.some(
                    (message) => message.indexOf(messageToSend) > -1
                  )
                ).toBe(false);
                done();
              } catch (error) {
                done(error);
              }
            });
          });
        });
      });
    });
  });
});
