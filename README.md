# Simple Chat Server

## Getting started

Simple chat server that listen on TCP port 10000 for clients. The chat protocol is very simple, clients connect with "telnet" and write single lines of text. On each new line of text, the server will broadcast that line to all other connected clients.

## Install

```
npm i
```

## Run

Default port is 10000 but you can specify another port using env variables (or .env file).
Server will start on localhost.

```
npm run start
```

## Test

```
npm run test
```
