import 'dotenv/config';
import log from 'loglevel';
import { ChatServer } from './src/chatServer/chatServer.js';

log.setLevel('info');
const port = process.env.SIMPLE_CHAT_SERVER_PORT || 10000;
const server = new ChatServer();

await server.start(port);
